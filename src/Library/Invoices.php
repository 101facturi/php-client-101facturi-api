<?php

namespace InvoicesClient\Library;

/**
 * PHP API client for 101facturi.
 *
 * Use this to generate invoices and estimates with 101facturi.ro or contabilitate-pfa.ro
 */

class Invoices extends Base {

  private $page = 1;
  private $size = 25;
  private $sortField = 'id';
  private $sortDirection = 'desc';
  private $callBasePath = '/invoices';

  private $filters = [];

  public function __construct() {

  }

  public function setPage($page) {
    $this->page = (int) $page;
    return $this;
  }

  public function setSize($size) {
    $this->size = (int) $size;
    return $this;
  }

  public function sortBy($field = 'id', $direction = 'desc') {
    if (in_array($field, $this->availableSortFields())) {
      $this->sortField = $field;
    }

    $this->sortDirection = $direction === 'asc' ? 'asc' : 'desc';

    return $this;
  }

  public function setFilters($filters) {
    foreach ($filters as $filter => $values) {
      if (!isset($this->availableFilters()[$filter])) {
        continue;
      }

      foreach ($values as $value) {
        $this->filters[$filter][] = $value;
      }
    }

    return $this;
  }

  public function removeFilters($filters) {
    foreach ($filters as $filter => $values) {
      if (!isset($this->availableFilters()[$filter])) {
        continue;
      }

      if (empty($values) || empty($this->filters[$filter])) {
        $this->filters[$filter] = [];
      } else foreach ($values as $value) {
        $this->filters[$filter] = array_filter($this->filters[$filter], function($v) use ($value) {
          return $value == $v;
        });
      }
    }

    return $this;
  }

  public function availableSortFields() {
    return [
      'id',
      'number',
    ];
  }

  public function availableFilters() {
    return [
      /* invoice ID */
      'id' => [],
      /* invoice series & number */
      'number' => [],
      /* client id */
      'client' => [],
      /* invoice dates */
      'from' => [],
      'until' => [],
    ];
  }

  public function getList() {
    return $this->request()->get($this->buildUrl());
  }

  private function buildUrl() {
    // http://101facturi.k/api/invoices/order-by-id/sort-desc/size-10/page-3
    return $this->callBasePath
        . '/order-by-' . $this->sortField
        . '/sort-' . $this->sortDirection
        . '/page-' . $this->page
        . '/size-' . $this->size
        . $this->stringifyFilter();
  }

  private function stringifyFilter() {
    $filters = '';

    foreach ($this->filters as $filter => $values) {
      $filters .= '/' . $filter . '/' . implode('/', $values);
    }

    return $filters;
  }
}
