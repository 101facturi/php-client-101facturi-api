<?php

namespace InvoicesClient\Library;

/**
 * PHP API client for 101facturi.
 *
 * Use this to generate invoices and estimates with 101facturi.ro or contabilitate-pfa.ro
 */

class Request extends Base {

  private $api = null;

  private $params = [
    'get' => [],
    'post' => [],
    'headers' => [],
  ];

  private $path = '';
  private $queryString = '';

  public function __construct() {
    $this->api = curl_init();
    $this->setHeaders();
  }

  public function __destruct() {
    $this->close();
  }

  public function setHeaders($headerList = []) {

    if (Config::get('token')) {
      $headerList = $headerList + ['api-token' => Config::get('token')];
    }

    curl_setopt($this->api, CURLOPT_HEADER, 0);

    $headers = [];
    foreach ($headerList as $header => $value) {
      if (is_array($value)) {
        foreach ($value as $headerItem) {
          $headers[] = "{$header}: {$headerItem}";
        }
      } else {
        $headers[] = "{$header}: {$value}";
      }
    }

    if ($headers) {
      curl_setopt($this->api, CURLOPT_HTTPHEADER, $headers);
    }

  }

  public function setPost($data = []) {
    if ($data) {
      curl_setopt($this->api, CURLOPT_POSTFIELDS, http_build_query($data));
    }
  }

  public function setGet($data = []) {
    if ($data) {
      $this->queryString = '/?' . http_build_query($data);
    }
  }

  public function reset() {
    curl_reset($this->api);
  }

  public function close() {
    if (!$this->api) {
      return;
    }
    curl_close($this->api);
  }

  public function get($path = '', $close = false) {
    curl_setopt($this->api, CURLOPT_URL, Config::$baseUrl . ($path ? ('/' . trim($path, '/')) : '') . $this->queryString);
    curl_setopt($this->api, CURLOPT_RETURNTRANSFER, TRUE);

    $response = curl_exec($this->api);
    $error = curl_error($this->api);

    if ($error != '') {
      $this->close();
      Log::add('InvoicesClient\Library\Request error: ' . print_r($error, true));
      return false;
    }

    $header_size = curl_getinfo($this->api, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);
    $result['http_code'] = curl_getinfo($this->api, CURLINFO_HTTP_CODE);

    $close ? $this->close() : $this->reset();

    if ($result['http_code'] > 399) {
      Log::add('InvoicesClient\Library\Request http-code: ' . $result['http_code']);
      return false;
    }

    $response = json_decode($response, true);
    if (json_last_error() !== JSON_ERROR_NONE) {
      Log::add('InvoicesClient\Library\Request json error: ' . json_last_error_msg());
      return false;
    }

    if (!isset($response['status']) || !isset($response['data']) || !isset($response['messages'])) {
      Log::add('InvoicesClient\Library\Request response structure invalid');
      return false;
    }

    return $response;
  }

  public function api() {

  }
}