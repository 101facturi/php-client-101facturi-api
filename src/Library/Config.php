<?php

namespace InvoicesClient\Library;

/**
 * PHP API client for 101facturi.
 *
 * Use this to generate invoices and estimates with 101facturi.ro or contabilitate-pfa.ro
 */

class Config {

//  public static $baseUrl = 'https://101facturi.ro/api';
  public static $baseUrl = 'http://101facturi.k/api';

  private static $token = '';

  private static $listSize = 25;

  public static function get($item) {
    if (!isset(static::$$item)) {
      return null;
    }

    return static::$$item;
  }

  public static function set($item, $value) {
    if (!isset(static::$$item)) {
      return null;
    }

    return static::$$item = $value;
  }
}