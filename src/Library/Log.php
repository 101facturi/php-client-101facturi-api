<?php

namespace InvoicesClient\Library;

/**
 * PHP API client for 101facturi.
 *
 * Use this to generate invoices and estimates with 101facturi.ro or contabilitate-pfa.ro
 */

class Log {

  public function add($message, $message_type = 0, $destination = null, $additional_headers = null) {
    error_log($message, $message_type, $destination, $additional_headers);
  }

}