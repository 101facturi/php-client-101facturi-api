<?php

namespace InvoicesClient\Library;

/**
 * PHP API client for 101facturi.
 *
 * Use this to generate invoices and estimates with 101facturi.ro or contabilitate-pfa.ro
 */

class Base {

  private static $api;

  protected function request() {
    if (!static::$api) {
      static::$api = new Request();
    }

    return static::$api;
  }

}